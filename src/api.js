import axios from 'axios'

export default { 
    baseURL: 'http://transportapi.com/v3/uk',
    apiKey: '4227e8171e0410f5a7681d48ac592e55',
    appId: '773811a4',
    getBusStops: function (lat, lon, type='bus_stop', limit=1000) {
        const URL = `${this.baseURL}/places.json?lat=${lat}&lon=${lon}&type=${type}&limit=${limit}&api_key=${this.apiKey}&app_id=${this.appId}`
        return axios.get(URL)
    }, 
    getStopDepartures: function (ATCOCode, group='route',nextbuses='yes', limit=20) {
        const URL = `${this.baseURL}/bus/stop/${ATCOCode}/live.json?group=${group}&nextbuses=${nextbuses}&limit=${limit}&&api_key=${this.apiKey}&app_id=${this.appId}`
        return axios.get(URL)
    }
};
